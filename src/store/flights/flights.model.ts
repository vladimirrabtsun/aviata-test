import {Airlines, Flights} from '@/assets/data/data.model';

export interface FlightsState {
    airlines: Airlines;
    flights?: Flights;
    error: boolean;
}
