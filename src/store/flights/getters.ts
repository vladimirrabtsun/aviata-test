import {GetterTree} from 'vuex';
import {RootState} from '@/store/store.model';
import {data} from '@/assets/data/data';
import {FlightsState} from '@/store/flights/flights.model';
import {Airlines, Flights} from '@/assets/data/data.model';

export const getters: GetterTree<FlightsState, RootState> = {
    flights(state): Flights {
        return state.flights || [];
    },
    airlines(state): Airlines {
        return state.airlines || [];
    },
    airlinesArr(): Array<{ code: string, name: string }> {
        return Object.keys(data.airlines).map((key) => {
            return {
                code: key,
                name: data.airlines[key],
            };
        });
    },
};
