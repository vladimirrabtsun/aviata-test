import {Module} from 'vuex';
import {RootState} from '@/store/store.model';
import {FlightsState} from './flights.model';
import {actions} from './actions';
import {mutations} from './mutations';
import {getters} from './getters';

export const state: FlightsState = {
    flights: [],
    airlines: [],
    error: false,
};

const namespaced = true;

export const flights: Module<FlightsState, RootState> = {
    namespaced,
    state,
    getters,
    actions,
    mutations,
};
