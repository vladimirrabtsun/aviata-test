import {RootState} from '@/store/store.model';
import {ActionTree} from 'vuex';
import {FlightsState} from './flights.model';
import {data} from '@/assets/data/data';
import {DataFlight, Flight} from '@/assets/data/data.model';
import {toSingleArray} from '@/store/flights/actions.helpers';
import {addMomentToItinerary} from '@/shared/date-maps';

export const actions: ActionTree<FlightsState, RootState> = {
    setFlights({ commit }): void {
        if (data && data.flights) {
            // To make for each flight flight.itineraries Itinerary[] instead Itinerary[][]
            // Also add additional parameters
            const flights: Flight[] = data.flights
                .map((iterableDataFlight: DataFlight) => ({
                    ...iterableDataFlight,
                    itineraries: toSingleArray(iterableDataFlight.itineraries)
                        .map(addMomentToItinerary),
                }));
            commit('setFlightsSuccess', flights);
        } else {
            commit('setFlightsFail');
        }
        if (data && data.airlines) {
            commit('setAirlinesSuccess', data.airlines);
        } else {
            commit('setAirlinesFail');
        }
    },
};
