import {DataItinerary} from '@/assets/data/data.model';

export function toSingleArray(itinerariesArrays: DataItinerary[][]): DataItinerary[] {
    const commonArray: DataItinerary[] = [];
    itinerariesArrays.forEach((iterableItinerariesArray: DataItinerary[]) => {
        iterableItinerariesArray.forEach((iterableItinerary: DataItinerary) => {
            commonArray.push(iterableItinerary);
        });
    });
    return commonArray;
}
