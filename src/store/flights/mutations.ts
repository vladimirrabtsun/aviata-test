import {MutationTree} from 'vuex';
import {FlightsState} from '@/store/flights/flights.model';
import {Airlines, Flights} from '@/assets/data/data.model';

export const mutations: MutationTree<FlightsState> = {
    setFlightsSuccess(state, payload: Flights): void {
        state.flights = payload;
    },
    setFlightsFail(state): void {
        state.error = true;
        state.flights = [];
    },
    setAirlinesSuccess(state, payload: Airlines): void {
        state.airlines = payload;
    },
    setAirlinesFail(state): void {
        state.error = true;
        state.airlines = [];
    },
};
