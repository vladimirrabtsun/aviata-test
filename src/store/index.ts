import Vue from 'vue';
import Vuex, {StoreOptions} from 'vuex';
import {RootState} from '@/store/store.model';
import {flights} from '@/store/flights';

Vue.use(Vuex);

const index: StoreOptions<RootState> = {
  state: {
    version: '1.0.0',
  },
  modules: {
    flights,
  },
};

export default new Vuex.Store<RootState>(index);
