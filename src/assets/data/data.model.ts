import {ItineraryWithMoment} from '@/shared/date-models';

export interface BaggageOption {
    unit: string;
    value: number;
}

export interface Segment {
    origin: string;
    airport_dest: string;
    arr_time_iso: string;
    dep_terminal: string;
    dep_time_iso: string;
    carrier_name: string;
    stop_locations: string[];
    dest_code: string;
    airport_dest_terminal: string;
    equipment: string;
    flight_num: string;
    stops: number;
    airport_origin: string;
    cabin: string;
    dep_time: string;
    dest: string;
    origin_code: string;
    baggage_options: BaggageOption[];
    arr_time: string;
    plane: string;
    services: {
        full_description: string;
        alt_text: string;
        code: string;
        description: string;
        title: string;
        default: string;
        solution: string;
        value: string;
        icon: string;
    };
    fare_basis_code: string;
    airport_origin_terminal: string;
    arr_terminal: string;
    carrier: string;
    fare_seats: number;
}

export interface DataItinerary {
    dep_date: string;
    hash: string;
    dir_index: number;
    price: {
        currency: string;
        amount: string;
        price_raw: number;
    };
    layovers: number[];
    arr_date: string;
    allowed_offers: string[];
    carrier_name: string;
    is_meta: boolean;
    segments: Segment[];
    stops: number;
    carrier: string;
    refundable: boolean;
    traveltime: number;
}

export interface DataFlight {
    itineraries: DataItinerary[][];
    price_details: {
        base_raw: number;
        modifiers: string;
        modifiers_raw: number;
        taxes: string;
        base: string;
        taxes_raw: number;
    };
    price: string;
    currency: string;
    bonus_usage: boolean;
    services: {
        [key: string]: {
            full_description: string;
            alt_text: string;
            code: string;
            description: string;
            title: string;
            default: string;
            solution: string;
            value: string;
            icon: string;
        };
    };
    price_raw: number;
    validating_carrier: string;
    id: string;
    has_meta: boolean;
    has_offers: boolean;
    best_time: number;
    bonus_accrual: boolean;
    bonus_accrual_details: null;
    bonus_usage_details: null;
    provider: string;
    refundable: boolean;
    provider_class: string;
}

export declare type Itinerary = ItineraryWithMoment;

export interface Flight {
    itineraries: Itinerary[];
    price_details: {
        base_raw: number;
        modifiers: string;
        modifiers_raw: number;
        taxes: string;
        base: string;
        taxes_raw: number;
    };
    price: string;
    currency: string;
    bonus_usage: boolean;
    services: {
        [key: string]: {
            full_description: string;
            alt_text: string;
            code: string;
            description: string;
            title: string;
            default: string;
            solution: string;
            value: string;
            icon: string;
        };
    };
    price_raw: number;
    validating_carrier: string;
    id: string;
    has_meta: boolean;
    has_offers: boolean;
    best_time: number;
    bonus_accrual: boolean;
    bonus_accrual_details: null;
    bonus_usage_details: null;
    provider: string;
    refundable: boolean;
    provider_class: string;
}

export declare type DataFlights = DataFlight[];

export declare type Flights = Flight[];

export interface Airline {
    [key: string]: string;
}

export declare type Airlines = Airline[];

export interface Data {
    airlines: { [key: string]: string; };
    flights: DataFlights;
}

export const currencies: { [key: string]: string } = {
    KZT: '₸',
};
