import {DataItinerary, Segment} from '@/assets/data/data.model';
import {Moment} from 'moment';
import {byDateAscSort, byDateDescSort} from '@/shared/date-sorts';
const moment = require('moment');

const locale = 'ru';

function div(val: number, by: number): number {
    return (val - val % by) / by;
}

export function getItineraryDepTime(itinerary: DataItinerary): Moment {
    const depTimes: Moment[] = itinerary.segments.map((iterableSegment: Segment) => {
        return moment(iterableSegment.dep_time_iso);
    });
    return depTimes.sort(byDateAscSort)[0];
}

export function getItineraryArrTime(itinerary: DataItinerary): Moment {
    const arrTimes: Moment[] = itinerary.segments.map((iterableSegment: Segment) => {
        return moment(iterableSegment.arr_time_iso);
    });
    return arrTimes.sort(byDateDescSort)[0];
}

export function getOriginCode(itinerary: DataItinerary): string {
    return itinerary.segments.sort((a: Segment, b: Segment) => {
        return moment(a.dep_time_iso).valueOf() - moment(b.dep_time_iso).valueOf();
    })[0].origin_code;
}

export function getDestCode(itinerary: DataItinerary): string {
    return itinerary.segments.sort((a: Segment, b: Segment) => {
        return moment(b.arr_time_iso).valueOf() - moment(a.arr_time_iso).valueOf();
    })[0].dest_code;
}

export function getTimeHumanReadable(datetime: Moment): string {
    return datetime
        .locale(locale)
        .format('HH:mm');
}

export function getDayHumanReadable(datetime: Moment): string {
    return datetime
        .locale(locale)
        .format('DD MMM, ddd')
        .replace('.', '');
}

export function getDiffHumanReadable(dep: Moment, arr: Moment): string {
    let m = '';
    let h = '';
    const minutes = arr.diff(dep, 'minutes');
    if (minutes < 1) {
        return '';
    }
    const hours: number = Math.floor(minutes / (60));
    if (hours < 1) {
        return `${minutes} м`;
    }
    const days: number = div(hours, 24);
    if (days < 1) {
        m = ((minutes - (hours * 60)) > 0) ? ' ' + (minutes - (hours * 60)).toString() + ' м' : '';
        return `${hours} ч${m}`;
    }
    m = ((minutes - (hours * 60)) > 0) ? ' ' + (minutes - (hours * 60)).toString() + ' м' : '';
    h = ((hours - (days * 24)) > 0) ? ' ' + (hours - (days * 24)).toString() + ' ч' : '';
    return `${days} д${h}${m}`;
}

export function getDaysDiffFromDepToArr(dep: Moment, arr: Moment): number {
    return arr.diff(dep, 'days');
}

export function whetherTheNextDayHasArrived(dep: Moment, arr: Moment): number {
    const depDay = dep.dayOfYear();
    const arrDay = arr.dayOfYear();
    if (depDay > arrDay) {
        return (moment().weeksInYear() - depDay) + arrDay;
    }
    return arrDay - depDay;
}

export function getSegmentsHumanReadable(itinerary: DataItinerary): string[] {
    const strings: string[] = [];
    const objects = itinerary.segments
        .sort((a: Segment, b: Segment) => {
            return moment(a.arr_time_iso).valueOf() - moment(b.arr_time_iso).valueOf();
        })
        .map((iterableSegment: Segment) => {
            return {
                origin: iterableSegment.origin,
                dep: moment(iterableSegment.dep_time_iso),
                arr: moment(iterableSegment.arr_time_iso),
            } as {
                origin: string;
                dep: Moment;
                arr: Moment;
            };
        });
    if (objects.length === 0) {
        return [];
    }
    if (objects.length === 1) {
        return [];
    }
    if (objects.length === 2) {
        return [
            'через ' + objects[1].origin + ', ' + getDiffHumanReadable(objects[0].arr, objects[1].dep),
        ];
    }
    return [
        'через ' + objects[1].origin + ', ' + getDiffHumanReadable(objects[0].arr, objects[1].dep),
        'через ' + objects[2].origin + ', ' + getDiffHumanReadable(objects[1].arr, objects[2].dep),
    ];
}
