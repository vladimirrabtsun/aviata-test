import {DataItinerary} from '@/assets/data/data.model';
import {ItineraryWithMoment} from '@/shared/date-models';
import {
    getDayHumanReadable, getDestCode, getDiffHumanReadable,
    getItineraryArrTime,
    getItineraryDepTime, getOriginCode, getSegmentsHumanReadable,
    getTimeHumanReadable, whetherTheNextDayHasArrived,
} from '@/shared/date-helpers';
import {Moment} from 'moment';

export function addMomentToItinerary(itinerary: DataItinerary): ItineraryWithMoment {
    const dep: Moment = getItineraryDepTime(itinerary);
    const originCode = getOriginCode(itinerary);
    const arr: Moment = getItineraryArrTime(itinerary);
    const destCode = getDestCode(itinerary);
    const segmentsStrings = getSegmentsHumanReadable(itinerary);
    return {
        ...itinerary,
        dep_time_moment: dep,
        dep_day_hr: getDayHumanReadable(dep),
        dep_time_hr: getTimeHumanReadable(dep),
        origin_code: originCode,
        arr_time_moment: arr,
        arr_day_hr: getDayHumanReadable(arr),
        arr_time_hr: getTimeHumanReadable(arr),
        dest_code: destCode,
        diff_days_hr: whetherTheNextDayHasArrived(dep, arr),
        diff_string_hr: getDiffHumanReadable(dep, arr),
        segments_strings_hr: segmentsStrings,
    } as ItineraryWithMoment;
}
