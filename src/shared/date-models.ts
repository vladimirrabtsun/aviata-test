import {Moment} from 'moment';
import {DataItinerary} from '@/assets/data/data.model';

export interface ItineraryWithMoment extends DataItinerary {
    dep_time_moment: Moment;
    dep_day_hr: string;
    dep_time_hr: string;
    origin_code: string;
    arr_time_moment: Moment;
    arr_day_hr: string;
    arr_time_hr: string;
    dest_code: string;
    diff_days_hr: number;
    diff_string_hr: string;
    segments_strings_hr: string[];
}
