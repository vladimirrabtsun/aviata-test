import {Moment} from 'moment';

export function byDateAscSort(a: Moment, b: Moment) {
    return a.valueOf() - b.valueOf();
}

export function byDateDescSort(a: Moment, b: Moment) {
    return b.valueOf() - a.valueOf();
}
